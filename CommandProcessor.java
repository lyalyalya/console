package concole;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//todo: можно попробовать сделать Singleton'ом - ибо нафик тебе несколько инстансов этого процессора
//todo: еще можно всё сделать статиком. Как тебе больше нравицо
public class CommandProcessor {
    private final Map<String, Command> commands;

    public CommandProcessor() {
        commands = new HashMap<>();
        commands.put("RENAME", new RenameFileCommand());
        commands.put("HELP", new Help());
        commands.put("EXIT", new Exit());
        commands.put("TOUCH", new CreateFileCommand());
        commands.put("MKDIR", new CreateDirectoryCommand());
        commands.put("RM", new DeleteCommand());
        commands.put("ZIP",new ZipCommand());
        commands.put("UNZIP",new UnzipCommand());
        commands.put("MV", new MoveFileCommand());
    }

    public void executeCommand(List<String> args) {
        if (!args.isEmpty() && commands.containsKey(args.get(0).toUpperCase())) {
            commands.get(args.get(0).toUpperCase()).execute(args.subList(1, args.size()));
        } else throw new IllegalArgumentException("Command not found");
    }

//todo: по хорошему лучше обернуть в Collections.unmodifableMap, чтобы клиенты твою внутреннюю мапу не могли модифицировать
    Map<String, Command> getCommands() {
        return commands;
    }
}
