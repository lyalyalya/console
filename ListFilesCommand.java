package concole;

import java.util.List;

public class ListFilesCommand implements Command{
    @Override
    public String getDescription() {
        return "Showing files in directory.\nSYNOPSIS\nls [folder name] - list files in target directory\n";
    }

    @Override
    public void execute(List<String> args) {

    }
}
